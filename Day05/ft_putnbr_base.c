/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/08 16:00:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/08 16:30:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	<unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_solve(int nbr, int base)
{
	if (nbr < 0)
	{
		ft_putchar('-');
		nbr = nbr * -1;
	}
	if (nbr >= base)
		ft_solve(nbr / base, base);
	if (nbr % base == 10)
		ft_putchar('A');
	else if (nbr % base == 11)
		ft_putchar('B');
	else if (nbr % base == 12)
		ft_putchar('C');
	else if (nbr % base == 13)
		ft_putchar('D');
	else if (nbr % base == 14)
		ft_putchar('E');
	else if (nbr % base == 15)
		ft_putchar('F');
	else
		ft_putchar((nbr % base) + 48);
}

void	ft_solve_2(int nbr, int base)
{
	if (nbr < 0)
	{
		ft_putchar('-');
		nbr = nbr * -1;
	}
	if (nbr >= base)
		ft_solve_2(nbr / base, base);
	if (nbr % base == 0)
		ft_putchar('p');
	else if (nbr % base == 1)
		ft_putchar('o');
	else if (nbr % base == 2)
		ft_putchar('n');
	else if (nbr % base == 3)
		ft_putchar('e');
	else if (nbr % base == 4)
		ft_putchar('y');
	else if (nbr % base == 5)
		ft_putchar('v');
	else if (nbr % base == 6)
		ft_putchar('i');
	else if (nbr % base == 7)
		ft_putchar('f');
}

int	ft_strcm(char *dest, char *base)
{
	int	i;

	i = 0;
	while (base[i])
	{
		if (base[i] == dest[i])
			i++;
		else
			return (0);
	}
	return (1);
}

void	ft_putnbr_base(int nbr, char *base)
{
	int	i;
	char	dest[2] = "01";
	char	dest1[8] = "poneyvif";
	char	dest2[10] = "0123456789";
	char	dest3[16] = "0123456789ABCDEF";

	i = 0;
	while (base[i])
		i++;
	if (i == 2 && ft_strcm(dest, base) == 1)
		ft_solve(nbr, i);
	else if (i == 8 && ft_strcm(dest1, base) == 1)
		ft_solve_2(nbr, i);
	else if (i == 10 && ft_strcm(dest2, base) == 1)
		ft_solve(nbr, i);
	else if (i == 16 && ft_strcm(dest3, base) == 1)
		ft_solve(nbr, i);
	ft_putchar('\n');
}
