/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/09 14:12:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/09 14:30:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putstr(char *str);

int	ft_strcmp(char *s1, char *s2)
{
	int	i;

	i = 0;
	while (s1[i] && s2[i] && s1[i] == s2[i])
		i++;
	return (s1[i] - s2[i]);
}

int	main(int argc, char **argv)
{
	int	i;
	int	k;
	int	j;
	char	*temp;

	if (argc > 1)
	{
		i = 1;
		while (i < argc && i + 1 < argc)
		{
			j = i;
			if (ft_strcmp(argv[i], argv[i + 1]) > 0)
			{
				temp = argv[i];
				argv[i] = argv[i + 1];
				argv[i + 1] = temp;
			}
			while ( j > 0 && j - 1 > 0)
			{
				if (ft_strcmp(argv[j - 1], argv[j]) > 0)
				{
					temp = argv[j];
					argv[j] = argv[j - 1];
					argv[j - 1] = temp;
				}
				j--;
			}
			i++;
		}
		k = 1;
		while (k < argc)
		{
			ft_putstr(argv[k]);
			ft_putchar('\n');
			k++;
		}		
	}
	return (0);
}
