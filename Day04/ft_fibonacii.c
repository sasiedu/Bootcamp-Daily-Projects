/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fibonacci.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/06 13:02:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/06 13:20:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_fibonacci(int index)
{
	int	k;
	int	i;
	int	j;
	int	l;

	l = index - 1;
	i = 0;
	j = 1;
	if (index < 0)
		return (-1);
	else if (index == 0)
		return (0);
	else if (index == 1)
		return (1);
	else
		while (l != 0)
		{
			k = i + j;
			i = j;
			j = k;
			l--;
		}
	return (k);
}
