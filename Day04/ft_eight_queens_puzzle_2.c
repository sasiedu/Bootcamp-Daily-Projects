/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eight_queens_puzzle_2.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/08 14:02:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/08 14:20:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	<unistd.h>

int	board[10];
int	count = 1;

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int n)
{
	if (n > 9)
	{
		ft_putnbr(n / 10);
		ft_putnbr(n % 10);
	}
	else
		ft_putchar(48 + n);
}

int	ft_abs(int value)
{
	if (value < 0)
		return (-value);
	else
		return (value);
}

void	ft_print(int n)
{
	int	i;
	int	j;

	i = 1;
	while (i <= n)
	{
		j = 1;
		while ( j <= n)
		{
			if (board[i] == j)
				ft_putchar(48 + j); /* print position of queen */
			j++;
		}
		i++;
	}
	ft_putchar(' ');
	ft_putnbr(count);
	count++;
	ft_putchar('\n');
}

/* checking for attacks */
int	ft_check(int row, int col)
{
	int	i;

	i = 1;
	while (i <= row - 1)
	{
		if (board[i] == col)
			return (0);
		else
			if (ft_abs(board[i] - col) == ft_abs(i - row))
				return (0);
	i++;
	}
	return (1); /* no attacks */
}

void	ft_solve(int row, int n)
{
	int	col;

	col = 1;
	while (col <= n)
	{
		if (ft_check(row, col) == 1)
		{
			board[row] = col; /* placing queen b'cos no attacks */
			if ( row == n) /* end of grid */
				ft_print(n); /* print out grid */
			else /* try next position */
				ft_solve(row + 1, n);
		}
		col++;
	}
}

void	ft_eight_queens_puzzle_2(void)
{
	int	n;

	n = 8;
	ft_solve(1, n);
}
