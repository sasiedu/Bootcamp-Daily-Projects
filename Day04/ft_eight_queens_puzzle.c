/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eight_queens_puzzle_2.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/07 21:02:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/07 21:39:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	<unistd.h>

int	board[8];
void	ft_print_out(int n);

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int	abs(int value)
{
	if (value < 0)
		return (-value);
	else
		return (value);
}

/* checks for attacks */
int	ft_check(int row, int col)
{
	int	i;

	i = 1;
	while ( i <= row - 1)
	{
		/* checking col and diagonals */
		if (board[i] == col)
			return (0);
		else
			if (abs(board[i] - col) == abs(i - row))
				return (0);
		i++;
	}
	return (1); /* safe, no attack */
}

void	ft_solve(int row, int n)
{
	int	col;

	col = 1;
	while (col <= n)
	{
		if (ft_check(row, col))
		{
			board[row] = col; /* no attack so placing queen */
			if (row == n)   /* dead end */
				ft_print_out(n); /* print the value(board) */
			else
				ft_solve(row + 1, n); /* try next position */
		}
	}
}

void	ft_print_out(int n)
{
	int	i;
	int	j;

	i = 1;
	while (i <= n)
	{
		j = 1;
		while (j <= n)
		{
			if (board[i] == j)
				ft_putchar(48 + j);
			j++;
		}
		i++;
	}
	ft_putchar('\n');
}

void	ft_eight_queens_puzzle_2(void)
{
	ft_solve(1, 8);
}

int	main()
{
	ft_eight_queens_puzzle_2();
	return (0);
}
