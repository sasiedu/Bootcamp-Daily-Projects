/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/04 08:33:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/04 08:45:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putnbr(int nb)
{
	int	i;

	i = '0' + nb;
	if (i <= '9')
		ft_putchar(i);
	else if (i > '9')
	{
		ft_putnbr(nb / 10);
		ft_putnbr(nb % 10);
		ft_putchar('\n');
	}
}
