/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/04 07:59:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/04 22:12:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_print(int j, int k, int l, int m)
{
	ft_putchar(j);
	ft_putchar(k);
	ft_putchar(' ');
	ft_putchar(l);
	ft_putchar(m);
	ft_putchar(',');
	ft_putchar(' ');
}

void	ft_print_comb2(void)
{
	int	j;
	int	k;
	int	l;
	int	m;

	j = '0';
	while (j <= '9')
	{
		k = '0';
		while ( k <= '8')
		{
			l = j;
			while (l <= '9')
			{
				m = k + 1;
				while (m <= '9')
				{
					ft_print(j, k, l, m);
					m++;
				}
				l++;
			}
			k++;
		}
		j++;
	}
	ft_putchar('\n');
}
